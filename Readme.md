# Readme
This project has one class.
The class can compare two objects with each other. 

It will report an list for which the fields differ. 

It is not possible (by design) to compare two different objects with each other. 

## javadoc

<section role="region">
<ul class="blockList">
<li class="blockList"><a id="method.detail">
<!--   -->
</a>
<h3>Method Detail</h3>
<a id="compare(java.lang.Object,java.lang.Object)">
<!--   -->
</a><a id="compare(T,T)">
<!--   -->
</a>
<ul class="blockListLast">
<li class="blockList">
<h4>compare</h4>
<pre class="methodSignature">public static&nbsp;&lt;T&gt;&nbsp;java.util.List&lt;<a href="ObjectsComparator.CompareResult.html" title="class in nl.ensignprojects.utils">ObjectsComparator.CompareResult</a>&gt;&nbsp;compare&#8203;(T&nbsp;current,
                                                                          T&nbsp;changed)</pre>
<div class="block">Compares to objects and returns a list of field names for which the values are not equal.</div>
<dl>
<dt><span class="paramLabel">Type Parameters:</span></dt>
<dd><code>T</code> - </dd>
<dt><span class="paramLabel">Parameters:</span></dt>
<dd><code>current</code> - </dd>
<dd><code>changed</code> - </dd>
<dt><span class="returnLabel">Returns:</span></dt>
<dd>a list of Compare results.</dd>
</dl>
</li>
</ul>
</li>
</ul>
</section>