package nl.ensignprojects.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.VarHandle;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@UtilityClass
public final class ObjectsComparator {

    /**
     * Compares to objects and returns a list of field names for which the values are not equal.
     *
     * @param current
     * @param changed
     * @param <T>
     * @return a list of Compare results.
     */
    public static <T> List<CompareResult> compare(T current, T changed) {
        final MethodHandles.Lookup lookup;
        try {
            lookup = MethodHandles
                    .privateLookupIn(current.getClass(), MethodHandles.lookup());
        } catch (IllegalAccessException e) {
            // won't happen
            return List.of();
        }

        return Stream.of(current.getClass().getDeclaredFields())
                .map(e -> {
                    try {
                        VarHandle varHandle = lookup.findVarHandle(current.getClass(), e.getName(), e.getType());
                        return new CompareResult(e.getName(), varHandle.get(current), varHandle.get(changed));
                    } catch (NoSuchFieldException | IllegalAccessException ex) {
                        // won't happen...
                    }
                    return null;
                })
                .filter(Objects::nonNull)
                .filter(CompareResult::valuesAreNotEqual)
                .collect(Collectors.toList());
    }

    @Getter
    @ToString
    @AllArgsConstructor
    public static class CompareResult {
        private String fieldName;
        private Object currentValue;
        private Object newValue;

        boolean valuesAreNotEqual() {
            return !currentValue.equals(newValue);
        }
    }
}
