package nl.ensignprojects.utils;

import lombok.extern.slf4j.Slf4j;
import nl.ensignprojects.utils.ObjectsComparator.CompareResult;
import nl.ensignprojects.utils.model.Car;
import org.junit.jupiter.api.Test;

import java.util.List;

@Slf4j
class ObjectsComparatorTest {

    @Test
    void testCompare() {
        log.info("Start with test");
        Car mercedes = new Car("Mercedes", "A class", 5, true, false);
        Car golf = new Car("Volkswagen", "Golf", 5, false, true);
        List<CompareResult> results = ObjectsComparator.compare(mercedes, golf);

        results.forEach(e -> log.info("{} is changed: {} -> {}", e.getFieldName(), e.getCurrentValue(), e.getNewValue()));
    }

}