package nl.ensignprojects.utils.model;

import lombok.Value;

@Value
public class Car {
    private final String brand;
    private final String type;
    private final Integer doors;
    private final Boolean automaticWindows;
    private final Boolean radio;
}
